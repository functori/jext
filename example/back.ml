open Jext

module U = Utils.Make(Ttypes)

module S = struct
  include Ttypes
  let handle_config = None
  let handle_request ~src ~id req f = match src, req with
    | `api _, A i -> f (Ok (Some (C i)))
    | `api _, B s -> f (Ok (Some (D s)))
    | `client, A i -> f (Ok (Some (E i)))
    | `client, B s -> f (Ok (Some (F s)))
    | _, G -> U.popup ~id ~base:"html/notification.html" G;
      (f (Ok None))
    | _ -> f (Error "don't answer client message")
end

include Background.Make(S)
